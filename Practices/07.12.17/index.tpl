<html>
  <head>
    <style>
      * {
        margin: 0;
        padding: 0;
      }
      html, body {
        height: 100%;
      }
      .container {
        display: flex;
        padding: 20px;
      }
      .message {
        margin-bottom: 10px;
        padding: 10px;
        background-color: #888;
      }
      .chat {
        height: 100%;
        background-color: #eee;
        flex-basis: 50%;
      }
      .in {
        flex-basis: 50%;
      }
    </style>
  </head>
  <body>
    <div class="container">
      <form method="post" class="in">
        <input name="name">
        <input type="text" name="message">
        <input type="submit">
      </form>
      <div class="chat">
        %for i in messages:
          <div class=message>
            <h4>
              {{i[0]}}
            </h4>
            {{i[1]}}
          </div>
        %end
      </div>
    </div>
  </body>
</html>
