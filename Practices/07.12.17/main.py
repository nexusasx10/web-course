from bottle import run, template, get, post, request


messages = []


@get('/')
def root():
    return template('index', messages=messages)


@post('/')
def root():
    name = request.forms.get('name')
    message = request.forms.get('message')
    messages.append((name, message))
    return template('index', messages=messages)


run(debug=True)
