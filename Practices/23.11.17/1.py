from bottle import route, error, run, request


@route('/')
def root():
    return '<h1>Hello world123</h1><br><a href="/about">about</a>'


@route('/about')
def about():
    return '''
    <b>Имя:</b> Андрей Шелудяков<br>
    <b>Возраст:</b> 21<br>
    <b>Деятельность:</b> студент<br>
    '''


@route('/hello')
def hello():
    name = request.query.get('name')
    return f'<h1>Hello, {name}</h1>'


@error(404)
def error(arg):
    return f'Ошибочка вышла...{arg}'


run(debug=True, reloader=True)
