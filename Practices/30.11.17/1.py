from bottle import route, error, run, request, response, template


@route('/')
def root():
    return '<h1>Hello world123</h1><br><a href="/about">about</a>'


@route('/about')
def about():
    return '''
    <b>Имя:</b> Андрей Шелудяков<br>
    <b>Возраст:</b> 21<br>
    <b>Деятельность:</b> студент<br>
    '''


@route('/hello')
def hello():
    return template('template', name=request.query.get('name'))


@error(404)
def error(arg):
    return f'Ошибочка вышла...\n{arg}'


run(debug=True, reloader=True)
