<!DOCTYPE HTML>
<html>
  <head>
    <title>
      Andrey Shell (beta) - Главная
    </title>
    <link type="image/png" rel="icon" href="/static/images/favicons/favicon16.png" sizes="16x16">
    <link type="image/png" rel="icon" href="/static/images/favicons/favicon32.png" sizes="32x32">
    <link type="image/png" rel="icon" href="/static/images/favicons/favicon64.png" sizes="64x64">
    <link type="image/png" rel="icon" href="/static/images/favicons/favicon128.png" sizes="128x128">
    <link type="text/css" rel="stylesheet" href="/static/content/grid.css">
    <link type="text/css" rel="stylesheet" href="/static/content/base.css">
      <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
  </head>
  <body>
    <div class="page-wrapper">
      <div class="header-container">
        <div class="header">
          <a class="nav-button" href="/">
            <div class="caption logo">Andrey Shell (beta)</div>
          </a>
          <div class="navigation">
            <a class="nav-button nav-button-current" href="/">
              <div class="caption">Главная</div>
              <div class="indicator"></div>
            </a>
            <a class="nav-button" href="/gallery">
              <div class="caption">Галерея</div>
              <div class="indicator"></div>
            </a>
            <a class="nav-button" href="/me">
              <div class="caption">Обо мне</div>
              <div class="indicator"></div>
            </a>
            <a class="nav-button" href="/about">
              <div class="caption">О сайте</div>
              <div class="indicator"></div>
            </a>
          </div>
        </div>
      </div>
      <div class="page-buffer">
      </div>
        <div class="dummy">
          <br>
          Тут пока ничего нет ¯\_(ツ)_/¯
        </div>
      <div class="page-buffer">
      </div>
    </div>
    <div class="footer-container">
      <div class="footer">
        <div class="footer-text">
          Andrey Shell &copy; 2017
        </div>
      </div>
    </div>
  </body>
</html>
