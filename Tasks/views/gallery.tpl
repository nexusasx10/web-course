<!DOCTYPE HTML>
<html>
  <head>
    <title>
      Andrey Shell (beta) - Галерея
    </title>
    <link type="image/png" rel="icon" href="/static/images/favicons/favicon16.png" sizes="16x16">
    <link type="image/png" rel="icon" href="/static/images/favicons/favicon32.png" sizes="32x32">
    <link type="image/png" rel="icon" href="/static/images/favicons/favicon64.png" sizes="64x64">
    <link type="image/png" rel="icon" href="/static/images/favicons/favicon128.png" sizes="128x128">
    <link type="text/css" rel="stylesheet" href="/static/content/grid.css">
    <link type="text/css" rel="stylesheet" href="/static/content/base.css">
      <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
  </head>
  <body>
    %include image-view comments=comments, likes=likes
    <div class="page-wrapper">
      <div class="header-container">
        <div class="header">
          <a class="nav-button" href="/">
            <div class="caption logo">Andrey Shell (beta)</div>
          </a>
          <div class="navigation">
            <a class="nav-button" href="/">
              <div class="caption">Главная</div>
              <div class="indicator"></div>
            </a>
            <a class="nav-button nav-button-current" href="/gallery">
              <div class="caption">Галерея</div>
              <div class="indicator"></div>
            </a>
            <a class="nav-button" href="/me">
              <div class="caption">Обо мне</div>
              <div class="indicator"></div>
            </a>
            <a class="nav-button" href="/about">
              <div class="caption">О сайте</div>
              <div class="indicator"></div>
            </a>
          </div>
        </div>
      </div>
      <div class="page-buffer">
      </div>
      <div class="content grid-container">
        <div class="col-ph-len-12 col-tb-len-5 col-dt-len-3 col-left">
          <div class="frame image-frame">
            <img class="photo" id="img-0" data-next="img-1" src="/static/images/thumbnails/img-0.jpg" data-original="/static/images/photos/img-0.jpg" alt="My photo" title="img-0.jpg">
            <div class="photo-description">
              Photo
            </div>
          </div>
        </div>
        <div class="col-ph-len-12 col-tb-len-5 col-dt-len-3">
          <div class="frame image-frame">
            <img class="photo" id="img-1" data-next="img-2" data-prev="img-0" src="/static/images/thumbnails/img-1.jpg" data-original="/static/images/photos/img-1.jpg" alt="My photo" title="img-1.jpg">
            <div class="photo-description">
              Night
            </div>
          </div>
        </div>
        <div class="col-ph-len-12 col-tb-len-5 col-dt-len-3">
          <div class="frame image-frame">
            <img class="photo" id="img-2" data-next="img-3" data-prev="img-1" src="/static/images/thumbnails/img-2.jpg" data-original="/static/images/photos/img-2.jpg" alt="My photo" title="img-2.jpg">
            <div class="photo-description">
             Mountain
            </div>
          </div>
        </div>
        <div class="col-ph-len-12 col-tb-len-5 col-dt-len-3 col-right">
          <div class="frame image-frame">
            <img class="photo" id="img-3" data-prev="img-2" src="/static/images/thumbnails/img-3.jpg" data-original="/static/images/photos/img-3.jpg" alt="My photo"  title="img-3.jpg">
            <div class="photo-description">
              Forest
            </div>
          </div>
        </div>
      </div>
      <div class="page-buffer">
      </div>
    </div>
    <div class="footer-container">
      <div class="footer">
        <div class="footer-text">
          Andrey Shell &copy; 2017
        </div>
      </div>
    </div>
    <script type="text/javascript" src="/static/scripts/gallery.js">
    </script>
    <script type="text/javascript" src="/static/scripts/ajax.js">
    </script>
    <script type="text/javascript">
      if (document.body.dataset) {
        function setBack() {
          var background = getCookie("back");
          if (background) {
              document.body.style.backgroundImage = "url({0})".replace("{0}", background);
          }
        }
        addEventHandler(
          document.getElementById('image-view-setback-button'),
          "click",
          function() {
            document.cookie = "back=" + getDisplay().src;
            setBack();
          }
        );
        setBack();
      }
    </script>
  </body>
</html>
