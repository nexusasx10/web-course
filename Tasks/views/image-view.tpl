<div class="image-view" id="image-view">
    <div class="image-view-header">
        <div class="image-view-button image-view-close-button" id="image-view-close-button" title="Закрыть окно просмотра">
        </div>
        <div class="image-view-button image-view-source-button" id="image-view-source-button" title="Показать оригинал в новой вкладке">
        </div>
    </div>
    <div class="image-view-content">
        <div class="image-view-button image-view-prev-button" id="image-view-prev-button" title="Предыдущее фото">
        </div>
        <img class="image-loading" id="image-loading" src="/static/images/controls/loading.svg">
        <div class="content">
            <img class="image-display" id="image-display">
            <div class="comment-block">
                <form class="comment-form" method="post">
                    <input class="author-field" type="text" name="author" placeholder="Имя" required>
                    <br>
                    <br>
                    <textarea class="message-field" name="message" placeholder="Комментарий" required></textarea>
                    <br>
                    <br>
                    <input type="submit" value="Отправить">
                    <br>
                    <br>
                    <input id="like_button" type="button" value="Лайки: {{likes}}" data-set="{{seted}}">
                </form>
                <div class="comment-list">
                    %for comment in comments:
                        <div class="comment">
                            <b>{{comment.author}}</b> {{comment.time.strftime('%X %x')}}
                            <p>{{comment.message}}</p>
                        </div>
                    %end
                </div>
            </div>
        </div>
        <div class="image-view-button image-view-next-button" id="image-view-next-button" title="Следующее фото">
        </div>
    </div>
</div>
