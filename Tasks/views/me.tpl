<!DOCTYPE HTML>
<html>
  <head>
    <title>
      Andrey Shell (beta) - Обо мне
    </title>
    <link type="image/png" rel="icon" href="/static/images/favicons/favicon16.png" sizes="16x16">
    <link type="image/png" rel="icon" href="/static/images/favicons/favicon32.png" sizes="32x32">
    <link type="image/png" rel="icon" href="/static/images/favicons/favicon64.png" sizes="64x64">
    <link type="image/png" rel="icon" href="/static/images/favicons/favicon128.png" sizes="128x128">
    <link type="text/css" rel="stylesheet" href="/static/content/grid.css">
    <link type="text/css" rel="stylesheet" href="/static/content/base.css">
      <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
  </head>
  <body>
    %include image-view comments=comments
    <div class="page-wrapper">
      <div class="header-container">
        <div class="header">
          <a class="nav-button" href="/">
            <div class="caption logo">Andrey Shell (beta)</div>
          </a>
          <div class="navigation">
            <a class="nav-button" href="/">
              <div class="caption">Главная</div>
              <div class="indicator"></div>
            </a>
            <a class="nav-button" href="/gallery">
              <div class="caption">Галерея</div>
              <div class="indicator"></div>
            </a>
            <a class="nav-button nav-button-current" href="/me">
              <div class="caption">Обо мне</div>
              <div class="indicator"></div>
            </a>
            <a class="nav-button" href="/about">
              <div class="caption">О сайте</div>
              <div class="indicator"></div>
            </a>
          </div>
        </div>
      </div>
      <div class="page-buffer">
      </div>
      <div class="content grid-container">
        <div class="col-ph-len-12 col-tb-len-5 col-dt-len-3 col-left">
          <div class="frame image-frame">
            <img class="photo" id="me" src="/static/images/thumbnails/me.jpg" data-original="/static/images/photos/me.jpg" alt="My photo" title="me.jpg">
            <div class="photo-description">
              My photo
            </div>
            <div class="photo-full">
            </div>
          </div>
        </div>
        <div class="col-ph-len-12 col-tb-len-12 col-dt-len-9 col-right">
          <div class="info frame text-frame">
            <div class="title">
              Обо мне:
            </div>
            <div class="info-text">
              <span class="label">Имя:</span>
              <span class="labeled">Шелудяков Андрей Владимирович</span>
            </div>
            <div class="info-text">
              <span class="label">Род занятий:</span>
              <span class="labeled">Студент</span>
            </div>
            <div class="info-text">
              <span class="label">Город:</span>
              <span class="labeled">Екатеринбург</span>
            </div>
            <div class="info-text">
              <span class="label">Место работы (учёбы):</span>
              <span class="labeled">ИЕНиМ УрФУ</span>
            </div>
            <div class="info-text">
              <span class="label">E-mail:</span>
              <a class="labeled" href="mailto:andrey.shellex.next@gmail.com" title="mailto:andrey.shellex.next@gmail.com">andrey.shellex.next@gmail.com</a>
            </div>
            <div class="info-text">
              <span class="label">Я Вконтакте:</span>
              <a class="labeled" href="https://vk.com/nexusasx10" title="https://vk.com/nexusasx10">nexusasx10</a>
            </div>
            <div class="info-text">
              <span class="label">Я на GitHub:</span>
              <a class="labeled" href="https://github.com/nexusasx10" title="https://github.com/nexusasx10">nexusasx10</a>
            </div>
            <div class="info-text">
              <span class="label">Я на Bitbucket:</span>
              <a class="labeled" href="https://bitbucket.org/nexusasx10" title="https://bitbucket.org/nexusasx10">nexusasx10</a>
            </div>
          </div>
        </div>
      </div>
      <div class="page-buffer">
      </div>
    </div>
    <div class="footer-container">
      <div class="footer">
        <div class="footer-text">
          Andrey Shell &copy; 2017
        </div>
      </div>
    </div>
    <script type="text/javascript" src="/static/scripts/gallery.js">
    </script>
    <script type="text/javascript" src="/static/scripts/ajax.js">
    </script>
  </body>
</html>
