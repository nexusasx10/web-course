# -*- coding: utf-8 -*-
import datetime
import random
import string

from bottle import route, request, view, post, get, response
from collections import defaultdict

SYMBOLS = string.ascii_letters + string.digits


visitors = {}
comments = []
likes = set()


class Visitor:
    def __init__(self, session_id):
        self.session_id = session_id
        self.view_count = 1
        # todo resource id


class Comment:
    def __init__(self, author, message, time):
        # todo валидация модели
        self.author = author
        self.message = message
        self.time = time


def generate_session_id():
    session_id = None
    while not session_id or session_id in visitors:
        session_id = ''.join(random.choice(SYMBOLS) for _ in range(16))
    return session_id


def update_statistics():
    session_id = request.get_cookie('session_id')
    if session_id in visitors:
        visitors[session_id].view_count += 1
    else:
        session_id = generate_session_id()
        visitors[session_id] = Visitor(session_id)
        response.set_cookie('session_id', session_id, max_age=3600)


@route('/')
@view('index')
def root():
    update_statistics()
    return dict()


@route('/about')
@view('about')
def about():
    update_statistics()
    return dict(visits=len(visitors))


@route('/me')
@view('me')
def about():
    update_statistics()
    return dict(comments=comments, likes=len(likes), seted=request.get_cookie('session_id') in likes)


@get('/gallery')
@view('gallery')
def gallery():
    update_statistics()
    return dict(comments=comments, likes=len(likes), seted=request.get_cookie('session_id') in likes)


@get('/get_like')
def get_like():
    return str(len(likes))


@get('/set_like')
def set_like():
    session_id = request.get_cookie('session_id')
    if session_id in visitors:
        likes.add(session_id)
    return str(len(likes))


@get('/remove_like')
def remove_like():
    session_id = request.get_cookie('session_id')
    if session_id in visitors and session_id in likes:
        likes.remove(session_id)
    return str(len(likes))


@post('/gallery')
@view('gallery')
def get_message():
    author = request.forms.get('author')
    message = request.forms.get('message')
    comments.append(Comment(author, message, datetime.datetime.now()))
    return dict(comments=comments, likes=len(likes), seted=request.get_cookie('session_id') in likes)
