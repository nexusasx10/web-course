function getCookie(name) {
  var matches = document.cookie.match(new RegExp(
    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
  ));
  return matches ? decodeURIComponent(matches[1]) : undefined;
}

function addEventHandler(target, event_id, handler) {
  if (target.addEventListener) {
    target.addEventListener(event_id, handler);
  }
  else if (target.attachEvent) {
    target.attachEvent("on" + event_id, handler);
  }
}

function getDisplay() {
  return document.getElementById('image-display');
}

function showImageView() {
  document.getElementById('image-view').style.display = "block";
}

function hideImageView() {
  document.getElementById('image-view').style.display = "none";
  document.cookie = "opened=none";
}

function displayPhoto(id) {
    photo_data = document.getElementById(id).dataset;
    if (photo_data) {
      photo = photo_data.original;
      getDisplay().dataset.currentID = id;
    } else {
      photo = document.getElementById(id).getAttribute('data-original')
    }
    document.cookie = "opened=" + id;
    showImageView();
    getDisplay().style.display = "none";
    document.getElementById('image-loading').style.display = "inline";
    getDisplay().src = photo;
    getDisplay().onload = function() {
      getDisplay().style.display = "inline";
      document.getElementById('image-loading').style.display = "none";
    }
}

function nextPhoto() {
  display = getDisplay();
  if (display.dataset) {
    next = document.getElementById(display.dataset.currentID).dataset.next;
    if (next) {
      displayPhoto(next);
    }
  }
}

function prevPhoto() {
  display = getDisplay();
  if (display.dataset) {
    prev = document.getElementById(display.dataset.currentID).dataset.prev;
    if (prev) {
      displayPhoto(prev);
    }
  }
}

function likePress(event) {
    if (event.target.dataset.set == "True") {
        event.target.dataset.set = "False";
        ajax.get(
            "/remove_like",
            "",
            function(a) {
                event.target.style.borderWidth = "1px";
                event.target.value = "Лайки: " + a;
            }
        );
    } else {
        event.target.dataset.set = "True";
        ajax.get(
            "/set_like",
            "",
            function(a) {
                event.target.style.borderWidth = "0";
                event.target.value = "Лайки: " + a;
            }
        );
    }
}
function likeCheck() {
    ajax.get(
        "/get_like",
        "",
        function(a) {
            document.getElementById('like_button').value = "Лайки: " + a;
        }
    );
}
setInterval(likeCheck, 1000);

if (document.getElementsByClassName) {
  photos = document.getElementsByClassName('photo');
}
else {
  photos = document.querySelectorAll('.photo');
}
for (var i = 0; i < photos.length; i++) {
  addEventHandler(
    photos[i],
    "click",
    function(event) {
      target = event.target || event.srcElement;
      displayPhoto(target.id);
    }
  );
}
addEventHandler(
  document.getElementById('like_button'), "click", likePress
);
addEventHandler(
  document.getElementById('image-view-close-button'), "click", hideImageView
);
addEventHandler(
  document.getElementById('image-view-source-button'),
  "click",
  function() {
    window.open(getDisplay().src, '_blank');
  }
);
if (document.body.dataset) {
  if (getCookie("opened") && getCookie("opened") != "none") {
    displayPhoto(getCookie("opened"));
  }
  addEventHandler(document.getElementById('image-view-prev-button'), "click", prevPhoto);
  addEventHandler(document.getElementById('image-view-next-button'), "click", nextPhoto);
  addEventHandler(
    document,
    "keydown",
    function(event) {
      if (event.code == "ArrowLeft") {
        prevPhoto();
      }
      else if (event.code == "ArrowRight") {
        nextPhoto();
      }
      else if (event.code == "Escape") {
        hideImageView();
      }
      else if (event.code == "F1") {
        event.preventDefault();
        alert("Стрелка влево - предыдущее фото\nСтрелка вправо - следующее фото\nEsc - закрыть");
      }
    }
  );
}
